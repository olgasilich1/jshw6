// forEach выполняет функцию к каждому эллементу массива в котором он вызван

const myArray = ['hello', 'world', 23, '23', null, {}];
const filterBy = function (array, type) {
    // const result = array.filter(function (item) {
    //     if (item === null && type === 'null') {
    //         return false
    //     }
    //     return (typeof item != type);
    // })
    // return result;
        return array.filter(function(item) {
            return  type !== "null" && item === null || typeof item !== type && item !== null})
}
console.log(filterBy(myArray,'null'));
